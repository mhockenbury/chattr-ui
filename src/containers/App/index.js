// @flow
import React, { Component } from 'react';
import { Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import { connect } from 'react-redux';
import { authenticate, unauthenticate, logout } from '../../actions/session';
import Home from '../Home';
import NotFound from '../../components/NotFound';
import Login from '../Login';
import Signup from '../Signup';
import history from '../../history';
import RouteAuthenticated from '../../components/RouteAuthenticated';
import RedirectAuthenticated from '../../components/RedirectAuthenticated';
import Sidebar from '../../components/Sidebar';
import Room from '../Room';

type Props = {
    authenticate: () => void,
    unauthenticate: () => void,
    isAuthenticated: boolean,
    willAuthenticate: boolean,
    logout: () => void,
    currentUserRooms: Array,
}

class App extends Component {
    componentDidMount() {
        const token = localStorage.getItem('token');

        if (token) {
            this.props.authenticate();
        } else {
            this.props.unauthenticate();
        }
    };

    props: Props

    handleLogout = router => this.props.logout(router);

    render() {
        const { isAuthenticated, willAuthenticate, currentUserRooms } = this.props;
      const authProps = { isAuthenticated, willAuthenticate };
    return (
        <ConnectedRouter history={history}>
                <div style={{ display: 'flex', flex: '1 100%' }}>
                    {isAuthenticated &&
                     <Sidebar
                         rooms={currentUserRooms}
                         onLogoutClick={this.handleLogout}
                     />
                    }
                <Switch>
                    <RouteAuthenticated exact path="/" component={Home} {...authProps} />
                    <RedirectAuthenticated path="/login" component={Login} {...authProps} />
                    <RedirectAuthenticated path="/signup" component={Signup} {...authProps} />
                    <RouteAuthenticated pattern="/r/:id" component={Room} {...authProps} />
                    <RedirectAuthenticated component={NotFound} />
                </Switch>
                </div>
        </ConnectedRouter>
    );
    }
}

export default connect(
    state => ({
        isAuthenticated: state.session.isAuthenticated,
        willAuthenticate: state.session.willAuthenticate,
        currentUserRooms: state.rooms.currentUserRooms,
    }),
    { authenticate, unauthenticate, logout }
)(App);
