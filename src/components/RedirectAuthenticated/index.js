// @flow
import React from 'react';
import { Route, Redirect } from 'react-router';

type Props = {
  component: any,
  pattern: string,
  exact?: boolean,
  isAuthenticated: boolean,
  willAuthenticate: boolean,
};

const RedirectAuthenticated = ({
  pattern,
  exact,
  isAuthenticated,
  willAuthenticate,
  component: Component,
}: Props) =>
  <Route
    exact={exact}
    pattern={pattern}
    render={(props) => {
      if (isAuthenticated) { return <Redirect to={{ pathname: '/' }} />; }
      if (willAuthenticate) { return null; }
      if (!willAuthenticate && !isAuthenticated) { return <Component {...props} />; }
      return null;
    }}
  />;

export default RedirectAuthenticated;
