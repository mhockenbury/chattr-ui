import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import { routerMiddleware } from 'react-router-redux';
import history from '../history';

const routerHistorymiddleware = routerMiddleware(history);

const middleWare = [thunk, routerHistorymiddleware];

const createStoreWithMiddleware = applyMiddleware(...middleWare)(createStore);

const store = createStoreWithMiddleware(reducers);

export default store;
